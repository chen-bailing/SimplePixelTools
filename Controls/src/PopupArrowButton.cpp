#include "PopupArrowButton.h"

wxIMPLEMENT_CLASS(PopupArrowButton, wxPopupTransientWindow);

enum
{
	NAVI_BUTTON_ID_RESET = 5000,
	NAVI_BUTTON_ID_MOVE_UP,
	NAVI_BUTTON_ID_MOVE_DOWN,
	NAVI_BUTTON_ID_MOVE_LEFT,
	NAVI_BUTTON_ID_MOVE_RIGHT,
};

BEGIN_EVENT_TABLE(PopupArrowButton, wxPopupTransientWindow)
	EVT_BUTTON(NAVI_BUTTON_ID_MOVE_UP, PopupArrowButton::OnButtonClick)
	EVT_BUTTON(NAVI_BUTTON_ID_MOVE_LEFT, PopupArrowButton::OnButtonClick)
	EVT_BUTTON(NAVI_BUTTON_ID_RESET, PopupArrowButton::OnButtonClick)
	EVT_BUTTON(NAVI_BUTTON_ID_MOVE_RIGHT, PopupArrowButton::OnButtonClick)
	EVT_BUTTON(NAVI_BUTTON_ID_MOVE_DOWN, PopupArrowButton::OnButtonClick)
	EVT_KEY_DOWN(PopupArrowButton::OnKeyDown)
	EVT_KILL_FOCUS(PopupArrowButton::OnLostFocus)
END_EVENT_TABLE()

PopupArrowButton::PopupArrowButton(wxWindow* pclsParent, wxWindowID iWinID) :
wxPopupTransientWindow(pclsParent, wxBORDER_SIMPLE)
{
	m_pclsGridSizer_RootSizer = new wxGridSizer(3, 3, 0, 0);
	// Create Bitmap-Button
	m_pclsBitmapButton_MoveUp = new wxBitmapButton(this, NAVI_BUTTON_ID_MOVE_UP, wxBitmap( wxT("RES_ID_BMP_BUTTON_IMAGE_MOVE_UP_NORMAL"), wxBITMAP_TYPE_PNG_RESOURCE ), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator, wxT("MOVE_UP"));
	m_pclsBitmapButton_MoveLeft = new wxBitmapButton(this, NAVI_BUTTON_ID_MOVE_LEFT, wxBitmap( wxT("RES_ID_BMP_BUTTON_IMAGE_MOVE_LEFT_NORMAL"), wxBITMAP_TYPE_PNG_RESOURCE ), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator, wxT("MOVE_LEFT"));
	m_pclsBitmapButton_ResetOffset = new wxBitmapButton(this, NAVI_BUTTON_ID_RESET, wxBitmap( wxT("RES_ID_BMP_BUTTON_IMAGE_RESET_OFFSET_NORMAL"), wxBITMAP_TYPE_PNG_RESOURCE ), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator, wxT("RESET"));
	m_pclsBitmapButton_MoveRight = new wxBitmapButton(this, NAVI_BUTTON_ID_MOVE_RIGHT, wxBitmap( wxT("RES_ID_BMP_BUTTON_IMAGE_MOVE_RIGHT_NORMAL"), wxBITMAP_TYPE_PNG_RESOURCE ), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator, wxT("MOVE_RIGHT"));
	m_pclsBitmapButton_MoveDown = new wxBitmapButton(this, NAVI_BUTTON_ID_MOVE_DOWN, wxBitmap( wxT("RES_ID_BMP_BUTTON_IMAGE_MOVE_DOWN_NORMAL"), wxBITMAP_TYPE_PNG_RESOURCE ), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator, wxT("MOVE_DOWN"));

	// Set ButtonImage
	_addButton(NULL);							_addButton(m_pclsBitmapButton_MoveUp);				_addButton(NULL);
	_addButton(m_pclsBitmapButton_MoveLeft);	_addButton(m_pclsBitmapButton_ResetOffset);			_addButton(m_pclsBitmapButton_MoveRight);
	_addButton(NULL);							_addButton(m_pclsBitmapButton_MoveDown);			_addButton(NULL);

	m_pclsBitmapButton_MoveUp->SetCanFocus(false);
	m_pclsBitmapButton_MoveLeft->SetCanFocus(false);
	m_pclsBitmapButton_ResetOffset->SetCanFocus(false);
	m_pclsBitmapButton_MoveRight->SetCanFocus(false);
	m_pclsBitmapButton_MoveDown->SetCanFocus(false);

	//Set sizer;
	SetSizer(m_pclsGridSizer_RootSizer);
	_resize();
	m_iID = iWinID;
	Layout();
}

PopupArrowButton::~PopupArrowButton()
{

}

void PopupArrowButton::_addButton(wxWindow* pclsControls)
{
	if(NULL != m_pclsGridSizer_RootSizer)
	{
		if(NULL != pclsControls)
		{
			m_pclsGridSizer_RootSizer->Add(pclsControls, 0, wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
		}
		else
		{
			m_pclsGridSizer_RootSizer->Add(0, 0, 1, 0, 5);
		}
	}
}

void PopupArrowButton::_resize(void)
{
	wxSize		clsNewSize;

	clsNewSize.SetWidth(m_pclsBitmapButton_MoveLeft->GetSize().GetWidth() +
						m_pclsBitmapButton_ResetOffset->GetSize().GetWidth() +
						m_pclsBitmapButton_MoveRight->GetSize().GetWidth());
	clsNewSize.SetHeight(m_pclsBitmapButton_MoveUp->GetSize().GetHeight() +
						m_pclsBitmapButton_ResetOffset->GetSize().GetHeight() +
						m_pclsBitmapButton_MoveDown->GetSize().GetHeight());

	SetSize(clsNewSize);
}

void PopupArrowButton::OnButtonClick(wxCommandEvent& clsEvent)
{

}

void PopupArrowButton::OnKeyDown(wxKeyEvent& clsEvent)
{

}

void PopupArrowButton::OnLostFocus(wxFocusEvent& clsEvent)
{
	Dismiss();
	clsEvent.Skip();
}
