//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "GeneralToolBar.h"

//=======================================================================//
//= Event table.													    =//
//=======================================================================//


//=======================================================================//
//= Function define.										            =//
//=======================================================================//
GeneralToolBar::GeneralToolBar(wxWindow* pclsParent, wxWindowID iID)
: ToolbarBase(pclsParent, iID)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxToolBar::AddTool(GENERAL_TOOL_NEW_TEXT_WINDOW, wxT("Text mode"), wxBITMAP_PNG(RES_ID_TOOL_NEW_TEXT_WINDOW), wxNullBitmap, wxITEM_NORMAL, wxT("Text mode"), wxT("Open a text work window."), nullptr);
	//wxToolBar::AddTool(GENERAL_TOOL_NEW_FONT_WINDOW, wxT("Font mode"), wxBITMAP_PNG(RES_ID_TOOL_NEW_FONT_WINDOW), wxNullBitmap, wxITEM_NORMAL, wxT("Font mode"), wxT("Open a font work window."), nullptr);
	wxToolBar::AddTool(GENERAL_TOOL_NEW_IMAGE_WINDOW, wxT("Image mode"), wxBITMAP_PNG(RES_ID_TOOL_NEW_IMAGE_WINDOW), wxNullBitmap, wxITEM_NORMAL, wxT("Image mode"), wxT("Open a image work window."), nullptr);
	wxToolBar::AddSeparator();
	wxToolBar::AddTool(GENERAL_TOOL_OPEN_WORK, wxT("Open"), wxBITMAP_PNG(RES_ID_TOOL_OPEN), wxNullBitmap, wxITEM_NORMAL, wxT("Open"), wxT("Open a saved work."), nullptr);
	wxToolBar::AddTool(GENERAL_TOOL_SAVE_WORK, wxT("Save"), wxBITMAP_PNG(RES_ID_TOOL_SAVE), wxNullBitmap, wxITEM_NORMAL, wxT("Save"), wxT("Save work to file."), nullptr);
	//wxToolBar::AddTool(GENERAL_TOOL_CLOSE_WORK, wxT("Close"), wxBITMAP_PNG(RES_ID_TOOL_CLOSE_WORKING), wxNullBitmap, wxITEM_NORMAL, wxT("Close work"), wxT("Close current work."), nullptr);
	wxToolBar::AddSeparator();
	wxToolBar::AddTool(GENERAL_TOOL_SETTINGS, wxT("Settings"), wxBITMAP_PNG(RES_ID_TOOL_SETTINGS), wxNullBitmap, wxITEM_NORMAL, wxT("Change settings"), wxT("Change tools settings."), nullptr);
	wxToolBar::AddTool(GENERAL_TOOL_START, wxT("Convert"), wxBITMAP_PNG(RES_ID_TOOL_GENERATE), wxNullBitmap, wxITEM_NORMAL, wxT("Start"), wxT("Start convert process."), nullptr);
	wxToolBar::AddTool(GENERAL_TOOL_CANCEL, wxT("Cancel"), wxBITMAP_PNG(RES_ID_TOOL_CANCEL), wxNullBitmap, wxITEM_NORMAL, wxT("Cancel"), wxT("Cancel convert processing."), nullptr);
	wxToolBar::AddTool(GENERAL_TOOL_EXPORT_DATA, wxT("Export"), wxBITMAP_PNG(RES_ID_TOOL_EXPORT_DATA), wxNullBitmap, wxITEM_NORMAL, wxT("Export data."), wxT("Export generate data to file."), nullptr);
	wxToolBar::EnableTool(GENERAL_TOOL_START, false);
	wxToolBar::EnableTool(GENERAL_TOOL_SAVE_WORK, false);
	wxToolBar::EnableTool(GENERAL_TOOL_CANCEL, false);
	wxToolBar::EnableTool(GENERAL_TOOL_EXPORT_DATA, false);
	wxToolBar::Realize();
}

GeneralToolBar::~GeneralToolBar(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	/* Do special nothing. */
}

void GeneralToolBar::Working(bool bIsWorking)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxToolBar::EnableTool(GENERAL_TOOL_START, bIsWorking);
	wxToolBar::EnableTool(GENERAL_TOOL_SAVE_WORK, bIsWorking);
}

void GeneralToolBar::OnProcessStart(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxToolBar::EnableTool(GENERAL_TOOL_SETTINGS, false);
	wxToolBar::EnableTool(GENERAL_TOOL_START, false);
	wxToolBar::EnableTool(GENERAL_TOOL_CANCEL, true);
	wxToolBar::EnableTool(GENERAL_TOOL_EXPORT_DATA, false);
	wxToolBar::Realize();
}

void GeneralToolBar::OnProcessStop(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxToolBar::EnableTool(GENERAL_TOOL_SETTINGS, true);
	wxToolBar::EnableTool(GENERAL_TOOL_START, true);
	wxToolBar::EnableTool(GENERAL_TOOL_CANCEL, false);
	wxToolBar::EnableTool(GENERAL_TOOL_EXPORT_DATA, true);
	wxToolBar::Realize();
}

