#include "FontViewGridCtrl.h"

wxIMPLEMENT_CLASS(FontViewGrid, wxControl);

BEGIN_EVENT_TABLE(FontViewGrid, wxControl)
	EVT_PAINT(FontViewGrid::OnPaint)
	EVT_ERASE_BACKGROUND(FontViewGrid::OnEraseBackground)
END_EVENT_TABLE()
FontViewGrid::FontViewGrid(wxWindow* pclsParent, wxWindowID iWinID, const wxFont& clsFont, const wxPoint& clsPosition, const int iRow, const int iColumn):
wxControl(pclsParent, iWinID, clsPosition, wxDefaultSize, wxNO_BORDER, wxDefaultValidator, wxT("FontViewGrid")),
m_clsCDC(this)
{
	SetGrid(6, 16);
}

FontViewGrid::~FontViewGrid(void)
{

}

void FontViewGrid::OnEraseBackground(wxEraseEvent& clsEvent)
{
	// Do nothing.
}

void FontViewGrid::OnPaint(wxPaintEvent& clsEvent)
{
	_repaint();
}

void FontViewGrid::SetText(const int iRowIdx, const int iColumnIdx, const wxChar& cText)
{
	if((iRowIdx < m_iRow) && (iColumnIdx < m_iColumn))
	{
        m_strText[(iRowIdx*m_iColumn)+iColumnIdx] = cText;
	}
}

void FontViewGrid::SetGrid(const int iRow, const int iColumn)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	int					iCharWidth, iCharHeight;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	m_iRow = iRow;
	m_iColumn = iColumn;

	// Resize text buffer.
    m_strText = wxString(' ', m_iRow*m_iColumn);

	// Character paint block size.
	iCharWidth = m_iCharWidth+4;
	iCharHeight = m_iCharHeight+4;

	// Recalculate best size.
	m_clsBestSize.SetWidth(((iCharWidth+1)*m_iColumn)+1);
	m_clsBestSize.SetHeight(((iCharHeight+1)*m_iRow)+1);
}

void FontViewGrid::SetViewFont(const wxFont& clsFont)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	//Get character max size.
	m_clsPaintFont = clsFont;
	if(m_clsCDC.IsOk())
	{
		m_clsCDC.SetFont(clsFont);
		m_clsCDC.GetTextExtent("W", &m_iCharWidth, &m_iCharHeight);
		if(m_iCharWidth < m_iCharHeight)
		{
			m_iCharWidth = m_iCharHeight;
		}
		// Update best size.
		SetGrid(m_iRow, m_iColumn);
	}
}

void FontViewGrid::_repaint(void)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	int					iPaintSizeWidth, iPaintSizeHeight;
	wxBitmap			clsPaintBitMap;
	wxBufferedDC		clsBufferedDC;
	int					iCharWidth, iCharHeight;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/

	// Character paint block size.
	iCharWidth = m_iCharWidth+4;
	iCharHeight = m_iCharHeight+4;

	// Grid area size.
	iPaintSizeWidth = ((iCharWidth+1)*m_iColumn)+1;
	iPaintSizeHeight = ((iCharHeight+1)*m_iRow)+1;

	// Create buffer image and DC object.
	clsPaintBitMap.Create(iPaintSizeWidth, iPaintSizeHeight);
	clsBufferedDC.Init(&m_clsCDC, clsPaintBitMap);
	clsBufferedDC.SetFont(m_clsPaintFont);

	// Paint back ground.
	clsBufferedDC.SetPen(*wxBLACK_PEN);
	clsBufferedDC.SetBrush(*wxWHITE_BRUSH);

	// Paint rim.
	clsBufferedDC.DrawRectangle(0, 0, iPaintSizeWidth, iPaintSizeHeight);

	// Paint grid.
    for(int iIdxRow=1; iIdxRow<m_iRow; iIdxRow++)
	{
		clsBufferedDC.DrawLine(iIdxRow*iCharHeight, 0, iIdxRow*iCharHeight, iPaintSizeHeight-1);
	}
	for(int iIdxCol=1; iIdxCol<m_iColumn; iIdxCol++)
	{
		clsBufferedDC.DrawLine(0, iIdxCol*iCharWidth, iPaintSizeWidth-1, iIdxCol*iCharWidth);
	}

}
