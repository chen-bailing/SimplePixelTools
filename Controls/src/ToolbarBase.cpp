//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "ToolbarBase.h"

//=======================================================================//
//= Marco declare.                                                      =//
//=======================================================================//

//=======================================================================//
//= Global variable definition                                          =//
//=======================================================================//
const wxSize G_TOOL_ICON_SIZE(20, 20);

//=======================================================================//
//= Event table.													    =//
//=======================================================================//

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
ToolbarBase::ToolbarBase(wxWindow* pclsParent, wxWindowID iID)
: wxToolBar(pclsParent, iID, wxDefaultPosition, wxDefaultSize, wxTB_FLAT|wxTB_NODIVIDER|wxTB_HORIZONTAL)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxToolBar::SetToolBitmapSize(G_TOOL_ICON_SIZE);
}

ToolbarBase::~ToolbarBase(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	/* Do special nothing. */
}

bool ToolbarBase::GetToolToggled(int iToolID)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	return wxToolBar::GetToolState(iToolID);
}

 void ToolbarBase::SetToggleTool(int iToolID, bool bToggle)
 {
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxToolBar::ToggleTool(iToolID, bToggle);
 }
