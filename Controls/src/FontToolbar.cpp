//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "FontToolBar.h"
#include "Settings.h"
#include "Debug.h"
#include <wx/fontenum.h>

//=======================================================================//
//= Event table.													    =//
//=======================================================================//
BEGIN_EVENT_TABLE(FontToolBar, ToolbarBase)
	EVT_TOOL	(FONT_TOOL_ID_BOLD,					FontToolBar::OnToolEvent)
	EVT_TOOL	(FONT_TOOL_ID_ITALIC,				FontToolBar::OnToolEvent)
	EVT_COMBOBOX(FONT_TOOL_ID_FONT_NAME,			FontToolBar::OnListChanged)
	EVT_SPINCTRL(FONT_TOOL_ID_FONT_SIZE,			FontToolBar::OnSpinChanged)
END_EVENT_TABLE()

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
FontToolBar::FontToolBar(wxWindow* pclsParent, wxWindowID iID)
: ToolbarBase(pclsParent, iID)
{
	// Font face list.
	m_pclsFontFaceList = new wxComboBox(this, FONT_TOOL_ID_FONT_NAME, wxEmptyString, wxDefaultPosition, wxSize(200, -1), 0, NULL, wxCB_READONLY );
	if(nullptr != m_pclsFontFaceList)
	{
		_enumFonts();
		if(m_pclsFontFaceList->GetCount() > 0)
		{
			m_pclsFontFaceList->SetSelection(0);
		}
	}
	wxToolBar::AddControl(m_pclsFontFaceList);
	// Font size spin.
	m_pclsFontSizeSpin = new wxSpinCtrl(this, FONT_TOOL_ID_FONT_SIZE, wxT("12"), wxDefaultPosition, wxSize(50, -1), wxSP_ARROW_KEYS, 8, 64, 12 );
	wxToolBar::AddControl( m_pclsFontSizeSpin );
	// Font style button.
	wxToolBar::AddTool(FONT_TOOL_ID_BOLD, wxT("Bold"), wxBITMAP_PNG(RES_ID_TOOL_IMAGE_FONT_STYLE_BOLD), wxNullBitmap, wxITEM_CHECK, wxT("Bold"), wxT("Bold font."), nullptr);
	wxToolBar::AddTool(FONT_TOOL_ID_ITALIC, wxT("Italic"), wxBITMAP_PNG(RES_ID_TOOL_IMAGE_FONT_STYLE_ITALIC), wxNullBitmap, wxITEM_CHECK, wxT("Italic"), wxT("Italic font."), nullptr);
	// Load work settings.
	_loadSettings();

	wxToolBar::Realize();
}

FontToolBar::~FontToolBar(void)
{
	/* Do special nothing. */
}

void FontToolBar::_enumFonts(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(nullptr != m_pclsFontFaceList)
	{
		wxFontEnumerator	clsEnumFonts;
		wxArrayString		strFontNames;

		strFontNames = clsEnumFonts.GetFacenames(wxFONTENCODING_DEFAULT,false);
		for(uint32_t i=0;i<strFontNames.size();i++)
		{
			if(0 != strFontNames[i].Find("@"))
			{
				m_pclsFontFaceList->Append(strFontNames[i]);
			}
		}
	}
}

void FontToolBar::_loadSettings(void)
{
	/* Load work settings. */
	m_pclsFontFaceList->SetValue(WorkSettingsInstance().PaintFont().GetName());
	m_pclsFontSizeSpin->SetValue(WorkSettingsInstance().PaintFont().GetSize());
	ToolbarBase::SetToggleTool(FONT_TOOL_ID_BOLD, GlobalPaintFont().IsBold());
	ToolbarBase::SetToggleTool(FONT_TOOL_ID_ITALIC, GlobalPaintFont().IsItalic());
}

void FontToolBar::_postPaintFontUpdateEvent(void)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxEvtHandler*			pclsParentEvtHandler = wxToolBar::GetParent();

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(pclsParentEvtHandler)
	{
		wxCommandEvent clsFontUpdateEvent(wxEVT_TOOL, wxToolBarBase::GetId());
		pclsParentEvtHandler->ProcessEvent(clsFontUpdateEvent);
	}
}

void FontToolBar::OnToolEvent(wxCommandEvent& clsEvent)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	bool					bFontSettingsUpdate = true;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	switch(clsEvent.GetId())
	{
	case FONT_TOOL_ID_BOLD:
		{
			GlobalPaintFont().SetBold(ToolbarBase::GetToolToggled(FONT_TOOL_ID_BOLD));
			break;
		}
	case FONT_TOOL_ID_ITALIC:
		{
			GlobalPaintFont().SetItalic(ToolbarBase::GetToolToggled(FONT_TOOL_ID_ITALIC));
			break;
		}
	default:
		{
			bFontSettingsUpdate = false;
			WRN_LOG("Ignore event(%d) process.", clsEvent.GetId());
		}
	}

	if(bFontSettingsUpdate)
	{
		_postPaintFontUpdateEvent();
	}
	clsEvent.Skip();
}

void FontToolBar::OnListChanged(wxCommandEvent& clsEvent)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(FONT_TOOL_ID_FONT_NAME == clsEvent.GetId())
	{
		DBG_LOG("Update font name %s.", m_pclsFontFaceList->GetValue());
		GlobalPaintFont().SetName(m_pclsFontFaceList->GetValue());
		_postPaintFontUpdateEvent();
	}
	clsEvent.Skip();
}

void FontToolBar::OnSpinChanged(wxSpinEvent& clsEvent)
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	bool					bFontSettingsUpdate = true;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	switch(clsEvent.GetId())
	{
	case FONT_TOOL_ID_FONT_SIZE:
		{
			GlobalPaintFont().SetSize(m_pclsFontSizeSpin->GetValue());
			break;
		}
	default:
		{
			bFontSettingsUpdate = false;
			WRN_LOG("Ignore event(%d) process.", clsEvent.GetId());
		}
	}

	if(bFontSettingsUpdate)
	{
		_postPaintFontUpdateEvent();
	}
	clsEvent.Skip();
}

void FontToolBar::OnProcessStart(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxToolBar::Enable(false);
	wxToolBar::Realize();
}

void FontToolBar::OnProcessStop(void)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	wxToolBar::Enable(true);
	wxToolBar::Realize();
}
