#ifndef _INCLUDE_CLASS_FONT_VIEW_GRID_H_
#define _INCLUDE_CLASS_FONT_VIEW_GRID_H_

#include <wx/control.h>
#include <wx/dcclient.h>
#include <wx/dcbuffer.h>
#include <wx/bitmap.h>

class FontViewGrid : public wxControl
{
	wxDECLARE_CLASS(FontViewGrid);
	wxDECLARE_NO_COPY_CLASS(FontViewGrid);

	DECLARE_EVENT_TABLE();

	private:
		wxString				m_strText;
		int						m_iRow;
		int						m_iColumn;
		int						m_iCharWidth;
		int						m_iCharHeight;
		wxSize					m_clsBestSize;
		wxFont					m_clsPaintFont;
		wxClientDC				m_clsCDC;

		void					_repaint(void);

	protected:
		virtual void			OnEraseBackground(wxEraseEvent& clsEvent);
		virtual void			OnPaint(wxPaintEvent& clsEvent);

	public:
    // Constructor/Destructor
								FontViewGrid(wxWindow* pclsParent, wxWindowID iWinID = wxID_ANY,
													const wxFont& clsFont = (*wxNORMAL_FONT),
													const wxPoint& clsPosition = wxDefaultPosition,
													const int iRow = 6, const int iColumn = 16);
								~FontViewGrid(void);
		void					SetText(const int iRowIdx, const int iColumnIdx, const wxChar& cText = wxT(' '));
		void					SetGrid(const int iRow, const int iColumn);
		void					SetViewFont(const wxFont& clsFont);

};

#endif // _INCLUDE_CLASS_FONT_VIEW_GRID_H_
