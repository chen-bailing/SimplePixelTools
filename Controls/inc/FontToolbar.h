#ifndef _INCLUDE_CLASS_FONT_TOOLS_BAR_H_
#define _INCLUDE_CLASS_FONT_TOOLS_BAR_H_
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "ToolbarBase.h"
#include <wx/combobox.h>
#include <wx/spinctrl.h>

enum FONT_TOOL_EX_ID
{
    FONT_TOOL_ID_HIGHEST = wxID_HIGHEST+3000,
    FONT_TOOL_ID_FONT_NAME,
    FONT_TOOL_ID_FONT_SIZE,
    FONT_TOOL_ID_BOLD,
    FONT_TOOL_ID_ITALIC,
    FONT_TOOL_ID_MAX
};

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class FontToolBar : public ToolbarBase
{
    wxDECLARE_NO_COPY_CLASS(FontToolBar);
	wxDECLARE_EVENT_TABLE();
private:
    wxComboBox*						m_pclsFontFaceList;
	wxSpinCtrl*						m_pclsFontSizeSpin;

	void							_enumFonts(void);
	void							_loadSettings(void);
	void							_postPaintFontUpdateEvent(void);

protected:
	virtual void					OnToolEvent(wxCommandEvent& clsEvent);
	virtual void					OnListChanged(wxCommandEvent& clsEvent);
	virtual void					OnSpinChanged(wxSpinEvent& clsEvent);

public:
									// Constructor/Destructor
									FontToolBar(wxWindow* pclsParent, wxWindowID iID = wxID_ANY);
									~FontToolBar(void);
	void							OnProcessStart(void);
	void							OnProcessStop(void);
};

#endif //_INCLUDE_CLASS_GENERAL_TOOLS_BAR_H_
