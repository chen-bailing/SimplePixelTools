#ifndef _INCLUDE_CLASS_OUTPUT_TEXT_CTRL_H_
#define _INCLUDE_CLASS_OUTPUT_TEXT_CTRL_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/string.h>
#include <wx/textctrl.h>
#include "Encoder.h"

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class OutputTextCtrl
: public wxTextCtrl
{
private:
	RasterizedDataArray		m_clsRasterizedDataArray;
    unsigned long           m_luAddress;

public:
    explicit                OutputTextCtrl(wxWindow *pclsParent, wxWindowID iID);
    virtual                 ~OutputTextCtrl(void);
	void					AppendData(const RasterizedMonoImage& clsData);
	void					AppendData(const RasterizedMonoChar& clsData);
	void					Clear(void);
	virtual bool			SaveAsTextFile(const wxString& cstrPath);
	virtual bool			SaveAsBinaryFile(const wxString& cstrPath);
	void                    SaveTextMap(const wxString& cstrPath);
	virtual bool			IsEmpty(void) const;
};

#endif // _INCLUDE_CLASS_OUTPUTTEXT_H_
