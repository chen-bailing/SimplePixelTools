#ifndef _INCLUDE_CLASS_TOOL_BAR_BASE_H_
#define _INCLUDE_CLASS_TOOL_BAR_BASE_H_
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/toolbar.h>

//=======================================================================//
//= Marco declare.                                                      =//
//=======================================================================//

//=======================================================================//
//= Global variable definition                                          =//
//=======================================================================//
extern const wxSize G_TOOL_ICON_SIZE;

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class ToolbarBase : public wxToolBar
{
    wxDECLARE_NO_COPY_CLASS(ToolbarBase);
protected:

public:
    // Constructor/Destructor
									ToolbarBase(wxWindow* pclsParent, wxWindowID iID = wxID_ANY);
									~ToolbarBase(void);
	virtual bool					GetToolToggled(int iToolID);
	virtual void					SetToggleTool(int iToolID, bool bToggle);
};

#endif //_INCLUDE_CLASS_TOOL_BAR_BASE_H_
