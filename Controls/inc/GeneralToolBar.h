#ifndef _INCLUDE_CLASS_GENERAL_TOOLS_BAR_H_
#define _INCLUDE_CLASS_GENERAL_TOOLS_BAR_H_
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "ToolbarBase.h"
#include <wx/string.h>

//=======================================================================//
//= Data type define.                                                   =//
//=======================================================================//
enum GENERAL_TOOL_ID
{
    GENERAL_TOOL_ID_HIGHEST = wxID_HIGHEST+2000,
    GENERAL_TOOL_NEW_TEXT_WINDOW = GENERAL_TOOL_ID_HIGHEST,
    GENERAL_TOOL_NEW_FONT_WINDOW,
    GENERAL_TOOL_NEW_IMAGE_WINDOW,
    GENERAL_TOOL_OPEN_WORK,
    GENERAL_TOOL_SAVE_WORK,
    GENERAL_TOOL_CLOSE_WORK,
    GENERAL_TOOL_SETTINGS,
    GENERAL_TOOL_START,
    GENERAL_TOOL_START_TEXT_CONV,
    GENERAL_TOOL_START_IMAGE_CONV,
    GENERAL_TOOL_CANCEL,
    GENERAL_TOOL_EXPORT_DATA,
    GENERAL_TOOL_ID_MAX
};

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class GeneralToolBar : public ToolbarBase
{
    wxDECLARE_NO_COPY_CLASS(GeneralToolBar);
protected:

public:
    // Constructor/Destructor
									GeneralToolBar(wxWindow* pclsParent, wxWindowID iID = wxID_ANY);
									~GeneralToolBar(void);
	void							Working(bool bIsWorking);
	void							OnProcessStart(void);
	void							OnProcessStop(void);
};

#endif //_INCLUDE_CLASS_GENERAL_TOOLS_BAR_H_
