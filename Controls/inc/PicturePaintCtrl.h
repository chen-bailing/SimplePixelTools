#ifndef _INCLUDE_CLASS_PICTURE_PAINT_CTRL_H_
#define _INCLUDE_CLASS_PICTURE_PAINT_CTRL_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/log.h>
#include <wx/sizer.h>
#include "wxLCDBase.h"
#include "Rasterizer.h"

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class PicturePaintCtrl: public wxLCDBase
{
	DECLARE_EVENT_TABLE();

	private:
		wxColour				m_clsPixelHColour;
		wxColour				m_clsPixelLColour;

	protected:
		virtual void			OnMouseEvent(wxMouseEvent& clsEvent);

	public:
								PicturePaintCtrl(wxWindow *pclsParent,
												wxWindowID iWinID = wxID_ANY,
												const wxSize& clsSizeInPixel = wxDefaultSizeInPixel);
								~PicturePaintCtrl(void);
		int						GetPixel(const int iPosX, const int iPosY);
		void					SetPixel(const int iPosX, const int iPosY, const int iValue);
		void					CleanScreen(void);
		void					SetPanelColour(const wxColour& clsPanelColour, bool bRefreshNow = true);
		void					SetPixelColour(const wxColour& clsPixelColour, bool bRefreshNow = true);
		const wxColour&			GetPixelHColour(void) const			{return m_clsPixelHColour;}
		const wxColour&			GetPixelLColour(void) const			{return m_clsPixelLColour;}
		wxBitmap				AsBitmap(void);
		bool					LoadBmpFile(const wxString& strPath);
		virtual void			SetPixelNumber(int iHorizontalPixelNumber, int iVerticalPixelNumber);
		virtual void			SetPixelNumber(const wxSize& clsNewSizeInPixelUnit);
};

#endif // _INCLUDE_CLASS_PICTURE_PAINT_CTRL_H_
