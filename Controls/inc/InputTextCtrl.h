#ifndef _INCLUDE_CLASS_INPUT_TEXT_CTRL_H_
#define _INCLUDE_CLASS_INPUT_TEXT_CTRL_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/string.h>
#include <wx/textctrl.h>

//=======================================================================//
//= Marco declare.                                                      =//
//=======================================================================//
#define EVT_TEXT_SEL(ID, FUNC) \
    DECLARE_EVENT_TABLE_ENTRY(wxEVT_TEXT_SEL, ID, -1, (wxObjectEventFunction)(wxEventFunction)(TextSelectFunc)&FUNC, (wxObject*)NULL),

//=======================================================================//
//= Data type declare.                                                  =//
//=======================================================================//
DECLARE_LOCAL_EVENT_TYPE(wxEVT_TEXT_SEL, evtID_TEXT_SEL)
class TextSelectEvent;
typedef void (wxEvtHandler::*TextSelectFunc)(TextSelectEvent&);

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class TextSelectEvent: public wxEvent
{
    DECLARE_DYNAMIC_CLASS(TextSelectEvent)

private:
    wxString					m_strText;
    long						m_lSelectionStart;
    long						m_lSelectionEnd;

public:
								TextSelectEvent(int iID = wxID_ANY, const wxString& clsText = wxEmptyString);
								TextSelectEvent(const TextSelectEvent& clsEvent);
    virtual TextSelectEvent*	Clone(void) const				{return new TextSelectEvent(*this);}
    void						SetText(const wxString& clsSelectionText){m_strText = clsSelectionText;}
    const wxString&				GetText(void) const 			{return m_strText;}
    void						SetSelectionStart(long lStart)	{m_lSelectionStart = lStart;}
	long						GetSelectionStart(void) const	{return m_lSelectionStart;}
    void						SetSelectionEnd(long lEnd)		{m_lSelectionEnd = lEnd;}
    long						GetSelectionEnd(void) const		{return m_lSelectionEnd;}

};

class InputTextCtrl
: public wxTextCtrl
{
	DECLARE_EVENT_TABLE();
private:
	bool						_judgeSelection(void);

protected:
	virtual void				OnCaptureMouneDown(wxMouseEvent& clsEvent);
	virtual void				OnCaptureMouneUp(wxMouseEvent& clsEvent);
	virtual void				OnMouseCaptureLost(wxMouseCaptureLostEvent& clsEvent);
	virtual void				OnIdle(wxIdleEvent& clsEvent);

public:
								InputTextCtrl(wxWindow *pclsParent, wxWindowID iID);
								~InputTextCtrl(void);
	bool						OpenFile(const wxString& cstrFilePath);
	void						Clear(void);
};

#endif // _INCLUDE_CLASS_OUTPUTTEXT_H_
