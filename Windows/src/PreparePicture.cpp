#include "PreparePicture.h"

OpenImageDialog::OpenImageDialog( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 800,320 ), wxDefaultSize );

	wxBoxSizer* pclsRootSizer = new wxBoxSizer( wxVERTICAL );

	wxBoxSizer* pclsImageSizer = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* pclsPerviewSizer  = new wxBoxSizer( wxHORIZONTAL );

	wxBoxSizer* pclsResourceImageSizer;
	pclsResourceImageSizer = new wxBoxSizer( wxVERTICAL );

	m_pclsResourceImageView = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxSize( 128,64 ), wxBORDER_SIMPLE|wxTAB_TRAVERSAL );
	pclsResourceImageSizer->Add( m_pclsResourceImageView, 0, wxALL, 5 );

	m_pclsImageInfoText = new wxStaticText( this, wxID_ANY, wxT("(None)"), wxDefaultPosition, wxDefaultSize, wxST_NO_AUTORESIZE|wxBORDER_SIMPLE );
	m_pclsImageInfoText->Wrap( -1 );
	m_pclsImageInfoText->SetMinSize( wxSize( 128,-1 ) );

	pclsResourceImageSizer->Add( m_pclsImageInfoText, 1, wxALL|wxEXPAND, 5 );


	pclsPerviewSizer->Add( pclsResourceImageSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* pclsImageWorkingSizer;
	pclsImageWorkingSizer = new wxBoxSizer( wxVERTICAL );

	m_pclsImagePerviewPanel = new wxScrolledWindow( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxBORDER_SIMPLE|wxHSCROLL|wxVSCROLL );
	m_pclsImagePerviewPanel->SetScrollRate( 5, 5 );
	pclsImageWorkingSizer->Add( m_pclsImagePerviewPanel, 1, wxEXPAND | wxALL, 5 );


	pclsPerviewSizer->Add( pclsImageWorkingSizer, 1, wxEXPAND, 5 );


	pclsImageSizer->Add( pclsPerviewSizer, 1, wxEXPAND, 5 );

	wxBoxSizer* pclsOptionSizer;
	pclsOptionSizer = new wxBoxSizer( wxVERTICAL );

	wxStaticText* pclsTitle2;
	pclsTitle2 = new wxStaticText( this, wxID_ANY, wxT("Image Size"), wxDefaultPosition, wxDefaultSize, 0|wxBORDER_SIMPLE );
	pclsTitle2->Wrap( -1 );
	pclsOptionSizer->Add( pclsTitle2, 0, wxALL|wxEXPAND, 5 );

	m_pclsRadioZoom = new wxRadioButton( this, wxID_ANY, wxT("Zoom picture to fixed size."), wxDefaultPosition, wxDefaultSize, wxRB_GROUP );
	m_pclsRadioZoom->SetValue( true );
	pclsOptionSizer->Add( m_pclsRadioZoom, 0, wxALL|wxEXPAND, 5 );

	m_pclsRadioCrop = new wxRadioButton( this, wxID_ANY, wxT("Crop a part from the picture."), wxDefaultPosition, wxDefaultSize, 0 );
	pclsOptionSizer->Add( m_pclsRadioCrop, 0, wxALL|wxEXPAND, 5 );

	wxBoxSizer* pclsImageSizeSizer;
	pclsImageSizeSizer = new wxBoxSizer( wxHORIZONTAL );

	wxStaticText* pclsTextW;
	pclsTextW = new wxStaticText( this, wxID_ANY, wxT("W:"), wxDefaultPosition, wxDefaultSize, 0 );
	pclsTextW->Wrap( -1 );
	pclsImageSizeSizer->Add( pclsTextW, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_pclsImageWidthSpinCtrl = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 75,-1 ), wxSP_ARROW_KEYS, 0, 10, 0 );
	pclsImageSizeSizer->Add( m_pclsImageWidthSpinCtrl, 0, wxALIGN_CENTER_VERTICAL, 5 );

	pclsTextH = new wxStaticText( this, wxID_ANY, wxT("H:"), wxDefaultPosition, wxDefaultSize, 0 );
	pclsTextH->Wrap( -1 );
	pclsImageSizeSizer->Add( pclsTextH, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );

	m_pclsImageHeightSpinCtrl = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 75,-1 ), wxSP_ARROW_KEYS, 0, 10, 0 );
	pclsImageSizeSizer->Add( m_pclsImageHeightSpinCtrl, 0, wxALIGN_CENTER_VERTICAL, 5 );


	pclsOptionSizer->Add( pclsImageSizeSizer, 0, wxEXPAND|wxRIGHT, 5 );

	m_pclsRadioResSize = new wxRadioButton( this, wxID_ANY, wxT("Same as input picture size."), wxDefaultPosition, wxDefaultSize, 0 );
	pclsOptionSizer->Add( m_pclsRadioResSize, 0, wxALL|wxEXPAND, 5 );


	pclsImageSizer->Add( pclsOptionSizer, 0, wxEXPAND, 5 );


	pclsRootSizer->Add( pclsImageSizer, 1, wxEXPAND, 5 );

	wxStaticLine* pclsStaticLine;
	pclsStaticLine = new wxStaticLine( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLI_HORIZONTAL );
	pclsRootSizer->Add( pclsStaticLine, 0, wxEXPAND | wxALL, 5 );

	wxBoxSizer* pclsButtonSizer;
	pclsButtonSizer = new wxBoxSizer( wxHORIZONTAL );

	m_pclsOKButton = new wxButton( this, wxID_ANY, wxT("OK"), wxDefaultPosition, wxDefaultSize, 0 );
	pclsButtonSizer->Add( m_pclsOKButton, 0, wxALL, 5 );

	m_pclsCancelButton = new wxButton( this, wxID_ANY, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	pclsButtonSizer->Add( m_pclsCancelButton, 0, wxALL, 5 );


	pclsRootSizer->Add( pclsButtonSizer, 0, wxALIGN_CENTER_HORIZONTAL, 5 );


	this->SetSizer( pclsRootSizer );
	this->Layout();

	this->Centre( wxBOTH );
}

OpenImageDialog::~OpenImageDialog()
{
}

void OpenImageDialog::_create(wxSizer* pclsRootSizer)
{

}
