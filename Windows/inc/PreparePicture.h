#ifndef _INCLUDE_CLASS_PREPARE_PICTURE_DIALOG_H_
#define _INCLUDE_CLASS_PREPARE_PICTURE_DIALOG_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/dialog.h>
#include <wx/scrolwin.h>
#include <wx/button.h>
#include <wx/bitmap.h>
#include <wx/dcclient.h>
#include <wx/dcmemory.h>
#include <wx/dcbuffer.h>
#include <wx/spinctrl.h>
#include <wx/stattext.h>
#include <wx/statline.h>
#include <wx/radiobut.h>
#include <wx/sizer.h>

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class OpenImageDialog : public wxDialog
{
	private:
		wxPanel* m_pclsResourceImageView;
		wxStaticText* m_pclsImageInfoText;
		wxRadioButton* m_pclsRadioZoom;
		wxRadioButton* m_pclsRadioCrop;
		wxSpinCtrl* m_pclsImageWidthSpinCtrl;
		wxStaticText* pclsTextH;
		wxSpinCtrl* m_pclsImageHeightSpinCtrl;
		wxRadioButton* m_pclsRadioResSize;
		wxButton* m_pclsOKButton;
		wxButton* m_pclsCancelButton;

		void					_create(wxSizer* pclsRootSizer);
		void					_initialize(void);
	protected:
		wxScrolledWindow* m_pclsImagePerviewPanel;

	public:

		OpenImageDialog( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Open a picture file"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 800,320 ), long style = wxDEFAULT_DIALOG_STYLE|wxRESIZE_BORDER );
		~OpenImageDialog();

};


#endif // _INCLUDE_CLASS_PREPARE_PICTURE_DIALOG_H_
