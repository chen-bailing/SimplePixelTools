#ifndef _INCLUDE_CLASS_STARTUP_PANEL_H_
#define _INCLUDE_CLASS_STARTUP_PANEL_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "GlobalEvents.h"
#include "Encoder.h"
#include <wx/aui/aui.h>
#include <wx/aui/tabmdi.h>
#include <wx/textctrl.h>
#include <wx/checkbox.h>
#include <wx/bmpbuttn.h>
#include <wx/textfile.h>
#include "sptChildWindowBase.h"

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class FontToolWindow
: public sptChildWindowBase
{
	DECLARE_EVENT_TABLE();

	private:
		// Controls
		wxTextCtrl*                     m_pclsTextEditor;
		wxButton*                       m_pclsOpenFileButton;
		wxButton*                       m_pclsDeduplicationButton;
		wxButton*                       m_pclsReorderButton;
		wxTextCtrl*                     m_pclsOutputFilePathText;
		wxBitmapButton*                 m_pclsSelectOutputButton;
		wxCheckBox*                     m_pclsBinaryCheck;
		wxFile*							m_pclsOutputTextFile;
		wxAuiMDIParentFrame*			m_pclsParentFrame;

	protected:
		virtual void					OnFontChanged(wxCommandEvent& clsEvent);
		virtual void					OnSettingChanged(wxCommandEvent& clsEvent);
		virtual void					OnOpenFile(wxCommandEvent& clsEvent);
		virtual void					OnDeduplication(wxCommandEvent& clsEvent);
		virtual void					OnReorder(wxCommandEvent& clsEvent);
        virtual void					OnSaveFile(wxCommandEvent& clsEvent);

	public:
										FontToolWindow(wxAuiMDIParentFrame * pclsParent = nullptr);
										~FontToolWindow(void);
		virtual bool					OnPrepareStart(void);
		virtual bool					OnProcessDone(void);
		virtual wxString				GetInput(void) const;
};

#endif // _INCLUDE_CLASS_STARTUP_PANEL_H_
