///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Aug 31 2019)
// http://www.wxformbuilder.org/
//
// PLEASE DO *NOT* EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef _INCLUDE_CLASS_MAIN_WINDOW_H_
#define _INCLUDE_CLASS_MAIN_WINDOW_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/frame.h>
#include <wx/toolbar.h>
#include <wx/aui/aui.h>
#include <wx/gauge.h>
#include <wx/thread.h>

#include "GeneralToolBar.h"
#include "FontToolBar.h"
#include "FontToolbarEx.h"
#include "GlobalEvents.h"
#include "OutputTextCtrl.h"
#include "sptWindowMgr.h"
#include "WorkThread.h"

//=======================================================================//
//= Marco declare.                                                      =//
//=======================================================================//


//=======================================================================//
//= Data type define.                                                   =//
//=======================================================================//
enum MAIN_CTRL_ID
{
    MAIN_CTRL_ID_LOWEST = wxID_HIGHEST+1000,
    GENERAL_TOOL_BAR = MAIN_CTRL_ID_LOWEST,
    FONT_TOOL_BAR,
    FONT_EX_TOOL_BAR,

    MAIN_CTRL_ID_HIGHEST
};

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class MainWindow
: public wxFrame
{
	DECLARE_EVENT_TABLE();

	private:
		wxAuiManager					m_clsLayoutMgr;
		sptWindowMgr*					m_pclsWindowMgr;
		GeneralToolBar*					m_pclsGeneralToolBar;
		FontToolBar*					m_pclsFontToolsBar;
		FontToolBarEx*					m_pclsFontToolsBarEx;
		wxGauge*						m_pclsProcessBar;
		OutputTextCtrl*					m_pclsOutput;
		wxString						m_strTextResource;

		void							_resetProcessBar(void);
		void							_processStart(void);
		void							_setProcessStatusBarText(const wxString& cstrText);
		bool							_openFile(void);
		bool							_newImageWindow(void);

	protected:
		virtual void					OnNew(wxCommandEvent& clsEvent);
		virtual void					OnOpen(wxCommandEvent& clsEvent);
		virtual void					OnSave(wxCommandEvent& clsEvent);
		virtual void					OnSettings(wxCommandEvent& clsEvent);
		virtual void					OnFontUpdate(wxCommandEvent& clsEvent);
		virtual void					OnSizeChanged(wxSizeEvent& clsEvent);
		virtual void					OnMaximize(wxMaximizeEvent& clsEvent);
		virtual void					OnClose(wxCloseEvent& clsEvent);
		virtual void					OnStart(wxCommandEvent& clsEvent);
		virtual void					OnCancel(wxCommandEvent& clsEvent);
		virtual void					OnExportData(wxCommandEvent& clsEvent);
		virtual void					OnWorkStart(WorkStartEvent& clsEvent);
		virtual void					OnWorkStop(WorkEndEvent& clsEvent);
		virtual void					TextConvNotice(TextConvEvent& clsEvent);
		virtual void					ImageConvNotice(ImageConvEvent& clsEvent);
		virtual void					WindowSelection(wxAuiNotebookEvent& clsEvent);
		virtual void					WindowClosed(wxAuiNotebookEvent& clsEvent);

	public:
										MainWindow(wxWindow* pclsParent = nullptr, wxWindowID iID = wxID_ANY);
										~MainWindow(void);
};

#endif /* _INCLUDE_CLASS_MAIN_WINDOW_H_ */

