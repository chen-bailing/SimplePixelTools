/***************************************************************
 * Name:      TextToolApp.cpp
 * Purpose:   Code for Application Class
 * Author:     ()
 * Created:   2018-01-16
 * Copyright:  ()
 * License:
 **************************************************************/
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "Application.h"
#include "Utility.h"
#include "Debug.h"
#include "Settings.h"
#include <wx/filename.h>

//=======================================================================//
//= Class definition                                                    =//
//=======================================================================//
IMPLEMENT_APP(SimplePixelToolsApp);

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
void SimplePixelToolsApp::_initializeLogTag(void)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxFileName              clsLogFileName;
	wxDateTime				clsNowTime;
	wxString                strLogFileName;

    /*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
    clsNowTime =            wxDateTime::Now();
    strLogFileName = wxString::Format(wxT("%04d%02d%02d_%02d%02d%02d.log"),
                                   clsNowTime.GetYear(), clsNowTime.GetMonth()+1, clsNowTime.GetDay(),
                                   clsNowTime.GetHour(), clsNowTime.GetMinute(), clsNowTime.GetSecond());
    clsLogFileName.SetPath(LOG_DIR);
    clsLogFileName.SetFullName(strLogFileName);

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
    delete wxLog::SetActiveTarget(new LogFile(clsLogFileName.GetFullPath()));
    wxLog::SetLogLevel(wxLOG_Debug);
	wxLog::EnableLogging(true);
}

void SimplePixelToolsApp::_loadConfig(void)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxFileName              clsConfigFileName;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
    clsConfigFileName.SetPath(CONFIG_FILE_PATH);
    clsConfigFileName.SetFullName(CONFIG_FILE_NAME);

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(true == clsConfigFileName.Exists())
    {
		if(false == ConfigurationInstance().Load(clsConfigFileName.GetFullPath()))
		{
            /* Load configuration file failed.. */
            wxLogError(wxT("Cannot open configuration file %s."), clsConfigFileName.GetFullPath());
        }
	}
	else
    {
    	wxLogError(wxT("Configuration file %s is NOT existed."), clsConfigFileName.GetFullPath());
    }
}

void SimplePixelToolsApp::_loadWorkSettings(void)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxFileName              clsConfigFileName;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
    clsConfigFileName.SetPath(CONFIG_FILE_PATH);
    clsConfigFileName.SetFullName(STARTUP_FILE_NAME);

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(true == clsConfigFileName.Exists())
    {
		if(false == WorkSettingsInstance().Load(clsConfigFileName.GetFullPath()))
		{
            /* Load configuration file failed.. */
            wxLogError(wxT("Cannot open work settings file %s."), clsConfigFileName.GetFullPath());
        }
	}
	else
    {
    	wxLogError(wxT("Work settings file %s is NOT existed."), clsConfigFileName.GetFullPath());
    }
}

bool SimplePixelToolsApp::OnInit()
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	/* Initialize log. */
	_initializeLogTag();

    /* Add image handler. */
    wxImage::AddHandler(new wxPNGHandler);
	wxImage::AddHandler(new wxJPEGHandler);

	/* Load config. */
    _loadConfig();
	_loadWorkSettings();

	/* Create a new main frame object. */
	m_pclsStartupFrame = new MainWindow();
    m_pclsStartupFrame->Show();

    return true;
}

int SimplePixelToolsApp::OnExit()
{
	/*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxFileName              clsConfigFileName;
	wxFileName              clsWorkSettingsFileName;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
    clsConfigFileName.SetPath(CONFIG_FILE_PATH);
    clsWorkSettingsFileName.SetPath(CONFIG_FILE_PATH);
    clsConfigFileName.SetFullName(CONFIG_FILE_NAME);
	clsWorkSettingsFileName.SetFullName(STARTUP_FILE_NAME);

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	ConfigurationInstance().Save(clsConfigFileName.GetFullPath());
	WorkSettingsInstance().Save(clsWorkSettingsFileName.GetFullPath());

	return wxAppBase::OnExit();
}
