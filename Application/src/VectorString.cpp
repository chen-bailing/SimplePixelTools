#include "VectorString.h"

wxVectorString::wxVectorString(void)
{

}

wxVectorString::wxVectorString(const wxString& cstrString)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	_dumpString(cstrString);
}

void wxVectorString::_dumpString(const wxString& clsString)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
    wxString::const_iterator    clsIter;

    /*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	clsIter =                   clsString.begin();

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	while(clsIter != clsString.end())
    {
        push_back(*clsIter);
        clsIter++;
    }
}

void wxVectorString::_copyFrom(const wxVectorString& clsSource)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
    wxVectorString::const_iterator clsIter;

    /*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	clsIter =                   clsSource.begin();

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	while(clsIter != clsSource.end())
    {
        push_back(*clsIter);
        clsIter++;
    }
}

wxVectorString::wxVectorString(const wxVectorString& clsSource)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	_copyFrom(clsSource);
}

void wxVectorString::Sort(void)
{
    wxVectorSort(*this);
}

void wxVectorString::Deduplication(void)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
    wxVectorString::iterator    clsIter;
    wxVectorString::iterator    clsDeduplicationIter;

    /*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	clsIter =                   begin();

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	while(clsIter != end())
    {
        clsDeduplicationIter = clsIter+1;
        while(clsDeduplicationIter!=end())
        {
            if((*clsDeduplicationIter) == (*clsIter))
            {
                erase(clsDeduplicationIter);
            }
            else
            {
                clsDeduplicationIter++;
            }
        }
        clsIter++;
    }
}

void wxVectorString::Remove(wxChar cRemoveChar)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
    wxVectorString::iterator    clsIter;

    /*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	clsIter =                   begin();

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	while(clsIter != end())
    {
        if(cRemoveChar == *clsIter)
        {
            erase(clsIter);
        }
        else
        {
            clsIter++;
        }
    }
}

wxVectorString& wxVectorString::operator=(const wxString& clsRight)
{
    /*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
     _dumpString(clsRight);

     return *this;
}

wxVectorString& wxVectorString::operator=(const wxVectorString& clsRight)
{
    /*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
    if(this != &clsRight)
    {
        _copyFrom(clsRight);
    }

    return *this;
}


wxString wxVectorString::ToString(void)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
    wxVectorString::const_iterator clsIter;
    wxString                    strReturn;

    /*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	clsIter =                   begin();

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	while(clsIter != end())
    {
        strReturn.Append(*clsIter);
        clsIter++;
    }

    return strReturn;
}

void wxVectorString::ToString(wxString& clsOutput)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
    wxVectorString::const_iterator clsIter;

    /*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
	clsIter =                   begin();

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	while(clsIter != end())
    {
        clsOutput.Append(*clsIter);
        clsIter++;
    }
}

