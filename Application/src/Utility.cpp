//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include "Utility.h"
#include <wx/sstream.h>

//=======================================================================//
//= Function define.										            =//
//=======================================================================//
void DeduplicationString(wxString& strSource)
{
	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	if(strSource.Length() > 1)
    {
        wxCharList              clsList;
        wxCharList::iterator    clsIter;
        wxString				strProcessedString;
        DumpStringToList(strSource, clsList);
        wxVectorSort(clsList);
        clsIter = clsList.begin();
        strProcessedString.Clear();
        while(clsIter != clsList.end())
        {
            strProcessedString.Append(*clsIter);
            clsIter++;
        }
        strSource = strProcessedString;
    }
}

bool DumpStringToList(const wxString& cstrSource, wxCharList& clsCharList)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	wxChar                  cChar = '\0';
	bool                    bReturn = true;

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	for(wxString::const_iterator clsIter = cstrSource.begin(); clsIter != cstrSource.end(); clsIter++)
    {
        cChar = *clsIter;
        clsCharList.push_back(cChar);
    }

    return bReturn;
}

bool DumpListToString(const wxCharList& clsSource, wxString& strDumpString)
{
    /*----------------------------------*/
	/* Variable Declaration				*/
	/*----------------------------------*/
	bool                    bReturn = true;

	/*----------------------------------*/
	/* Initialize						*/
	/*----------------------------------*/
    strDumpString.Clear();

	/*----------------------------------*/
	/* Process							*/
	/*----------------------------------*/
	for(wxCharList::const_iterator clsIter = clsSource.begin(); clsIter != clsSource.end(); clsIter++)
    {
        strDumpString.append(*clsIter);
    }

    return bReturn;
}


