#ifndef _INCLUDE_UTILITY_H_
#define _INCLUDE_UTILITY_H_
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/colour.h>
#include <wx/string.h>
#include <wx/vector.h>
#include "GlobalEvents.h"

//=======================================================================//
//= Marco declare.                                                      =//
//=======================================================================//
#define BYTE0_OF_INT32(INT)				((INT)&(0x000000FF))
#define BYTE1_OF_INT32(INT)				((INT>>8)&(0x000000FF))
#define BYTE2_OF_INT32(INT)				((INT>>16)&(0x000000FF))
#define BYTE3_OF_INT32(INT)				((INT>>24)&(0x000000FF))

#define CSS_LIKE_COLOR(WX_COLOR)		((BYTE0_OF_INT32(WX_COLOR.GetRGB()))|(BYTE1_OF_INT32(WX_COLOR.GetRGB())<<8)|(BYTE2_OF_INT32(WX_COLOR.GetRGB())<<16))

//=======================================================================//
//= Data type declare.                                                  =//
//=======================================================================//
enum /* Ctrl ID */
{
	ID_TOOL_NONE = wxID_HIGHEST+1,
	CTRL_ID_GENERAL_TOOLBAR,
	CTRL_ID_FONT_TOOLBAR,
	ID_CTRL_OFFSET_PANEL,

	ID_CTRL_TEXT_INPUT,
	ID_CTRL_OPEN_FILE,
	ID_TOOL_SAVE_FILE,
	ID_CTRL_DEDUPLICATION,
	ID_CTRL_SORT,
	ID_CTRL_TEXT_EDITOR,
	ID_CTRL_SAVE_INDEX_FILE,

	ID_TOOL_IMAGE_NEW,
	ID_TOOL_IMAGE_OPEN,
	ID_TOOL_IMAGE_SAVE,
	ID_TOOL_IMAGE_IMPORT,
	ID_TOOL_IMAGE_RESIZE,

	ID_TOOL_IMAGE_POINT,
	ID_TOOL_IMAGE_PAINT_LINE,
	ID_TOOL_IMAGE_PAINT_RECT,
	ID_TOOL_IMAGE_PAINT_CIR,

	// For setting dialog
	ID_CTRL_SETTING_SAVE_CFG,
	IC_CTRL_SETTING_REMOVE_CFG,
	ID_CTRL_SETTING_PANEL_COLOUR,
	ID_CTRL_SETTING_PIXEL_COLOUR,
	ID_CTRL_SETTING_GRID_COLOUR,
	ID_CTRL_SETTING_SCAN_ORDER_CHOICE,
	ID_CTRL_SETTING_DIRECTION_CHOICE,
	ID_CTRL_SETTING_BIT_ORDER_CHOICE,
	ID_CTRL_SETTING_CFG_LIST,
	ID_CTRL_SETTING_PERVIEW_TIMER,
	ID_CTRL_SETTING_COMMENT_SETTING,
	ID_CTRL_SETTING_COMMENT_IS_NEWLINE,

	ID_WORK_THREAD,
	ID_THREAD_TEXT_FILE_CONVERT
};

enum GLOBAL_EVT
{
    EVT_SETTING_CHANGED = wxID_HIGHEST+9000,
    EVT_START_CONVERT,
	EVT_PAINT_FONT_CHANGED,
    EVT_ID_MAX
};

typedef wxVector<wxChar>        wxCharList;

void DeduplicationString(wxString& strSource);
bool DumpStringToList(const wxString& cstrSource, wxCharList& clsCharList);
bool DumpListToString(const wxCharList& clsCharList, wxString& cstrSource);
int StrToInt(const wxString& cstrSource);

#endif // _INCLUDE_UTILITY_H_
