#ifndef _INCLUDE_VECTOR_STRING_H_
#define _INCLUDE_VECTOR_STRING_H_
//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/string.h>
#include <wx/vector.h>

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class wxVectorString : public wxVector<wxChar>
{
    private:

        void                _dumpString(const wxString& clsString);
        void                _copyFrom(const wxVectorString& clsSource);
    protected:

    public:
                            wxVectorString(void);
                            wxVectorString(const wxString& cstrString);
                            wxVectorString(const wxVectorString& clsSource);
        void                Sort(void);
        void                Deduplication(void);
        void                Remove(wxChar cRemoveChar);
        wxVectorString&     operator=(const wxString& clsRight);
        wxVectorString&     operator=(const wxVectorString& clsRight);
        wxString            ToString(void);
        void                ToString(wxString& claOutput);
};


#endif // _INCLUDE_VECTOR_STRING_H_
