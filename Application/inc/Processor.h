#ifndef _INCLUDE_PROCESSOR_H_
#define _INCLUDE_PROCESSOR_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/string.h>
#include <wx/image.h>

//=======================================================================//
//= Data type definition.                                               =//
//=======================================================================//
typedef enum _e_processor_data_type_
{
	INVALID_DUMP_DATA = 0,
	TEXT_DUMP_DATA,
	IMAGE_DUMP_DATA,
	DUMY_DATA_TYPE_MAX
}DUMP_DATA_TYPE;

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class DumpData
{
private:
	DUMP_DATA_TYPE				m_eDataType;
public
								ProcessorData(DUMP_DATA_TYPE etype) {m_eDataType = etype;}
};

class TextDumpData : public DumpData
{
private:
	wxString					m_strText;

public:
								TextDumpData(const wxString& cstrText = wxEmptyString) {m_strText = cstrText;}
								TextDumpData(const TextDumpData& clsSource) {m_strText = clsSource.Get();}
	const wxString&				Get(void) const {return m_strText;}
};

class ImageDumpData : public DumpData
{
private:
	wxImage						m_clsImage;

public:
								ImageDumpData(const m_clsImage& clsImage = wxNullImage) {m_clsImage = clsImage;}
								ImageDumpData(const ImageDumpData& clsSource) {m_clsImage = clsSource.Get();}
	const wxImage&				Get(void) const {return m_clsImage;}
};

class DumpDataManager
{
private:
	DumpData*					m_pclsDumpData;

public:
								DumpDataManager(void);
								DumpDataManager(const TextDumpData& clsTextData);
								DumpDataManager(const ImageDumpData& clsImageData);
	DUMP_DATA_TYPE				GetType(void);

};


#endif // _INCLUDE_PROCESSOR_H_
