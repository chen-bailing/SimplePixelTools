#ifndef _INCLUDE_DEBUG_MODULE_H_
#define _INCLUDE_DEBUG_MODULE_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/log.h>
#include <wx/file.h>
#include <wx/string.h>
#include <wx/log.h>

//=======================================================================//
//= Marco declare.                                                      =//
//=======================================================================//
#define LOG_DIR                         (wxT("../logs"))

#define	INF_LOG(S, ...)					wxLogInfo(wxT("[%s][%d]" S), __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define	DBG_LOG(S, ...)					wxLogDebug(wxT("[%s][%d]" S), __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define	WRN_LOG(S, ...)					wxLogWarning(wxT("[%s][%d]" S), __FUNCTION__, __LINE__, ##__VA_ARGS__)
#define	ERR_LOG(S, ...)					wxLogError(wxT("[%s][%d]" S), __FUNCTION__, __LINE__, ##__VA_ARGS__)

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class LogFile:public wxLog
{
    private:
        wxFile                  m_clsLogFile;
    protected:
        virtual void            DoLogText(const wxString& cstrLogMsg);
        virtual void            DoLogTextAtLevel(wxLogLevel eLevel, const wxString & cstrLogMsg);

    public:
                                LogFile(const wxString& cstrFileName);
        virtual                 ~LogFile(void);
};

#endif // _INCLUDE_DEBUG_MODULE_H_
