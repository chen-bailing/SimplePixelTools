#ifndef _INCLUDE_CLASS_APPLICATION_H_
#define _INCLUDE_CLASS_APPLICATION_H_

//=======================================================================//
//= Include files.													    =//
//=======================================================================//
#include <wx/app.h>
#include <wx/log.h>
#include "MainWindow.h"

//=======================================================================//
//= Class declare.                                                      =//
//=======================================================================//
class SimplePixelToolsApp : public wxApp
{
    private:
        MainWindow*			m_pclsStartupFrame;

        void				_loadConfig(void);
		void				_loadWorkSettings(void);
		void                _initializeLogTag(void);

    public:
        virtual bool		OnInit();
        virtual int			OnExit();
};

#endif // _INCLUDE_CLASS_PIXEL_TEXT_TOOL_APP_H_
